# SPDX-FileCopyrightText: 2023 Birger Schacht
# SPDX-License-Identifier: MIT

from . import Directive


class Bold(Directive):
    regex = r"'''(?P<text>.*?)'''"

    def newvalue(self, matchobj):
        text = matchobj.group('text')
        return f'**{text}**'
