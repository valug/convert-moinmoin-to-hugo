import re


class Directive:
    regex = None
    flags = 0

    def getregex(self):
        return re.compile(self.regex, self.flags)

    def replace(self, content):
        return re.sub(self.getregex(), self.newvalue, content)

    def newvalue(self, matchobj):
        raise NotImplementedError
