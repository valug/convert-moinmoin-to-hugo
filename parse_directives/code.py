# SPDX-FileCopyrightText: 2023 Birger Schacht
# SPDX-License-Identifier: MIT

import re

from . import Directive


class Code(Directive):
    regex = r'\{\{\{(?P<code>.*?)\}\}\}'
    flags = re.DOTALL

    def newvalue(self, matchobj):
        code = matchobj.group('code')
        return f'<code>{code}</code>'
