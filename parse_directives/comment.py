# SPDX-FileCopyrightText: 2023 Birger Schacht
# SPDX-License-Identifier: MIT

import re

from . import Directive


class Comment(Directive):
    regex = r"^##+(?P<text>.*)$"
    flags = re.MULTILINE

    def newvalue(self, matchobj):
        text = matchobj.group('text')
        return f'{{{{% comment %}}}}{text}{{{{% /comment %}}}}'
