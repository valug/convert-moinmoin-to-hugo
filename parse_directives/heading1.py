# SPDX-FileCopyrightText: 2023 Birger Schacht
# SPDX-License-Identifier: MIT

import re

from . import Directive


class Heading1(Directive):
    regex = r'^= (?P<heading>.*?) =$'
    flags = re.MULTILINE

    def newvalue(self, matchobj):
        heading = matchobj.group('heading')
        return f'# {heading}'
