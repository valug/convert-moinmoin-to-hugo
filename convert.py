#!/usr/bin/python3
# SPDX-FileCopyrightText: 2023 Birger Schacht
# SPDX-License-Identifier: MIT

import datetime
import pathlib
import re
import sys
import urllib

from parse import Parser

if len(sys.argv) == 3:
    indir = sys.argv[1]
    outdir = pathlib.Path(sys.argv[2])
    outdir.mkdir(parents=True, exist_ok=True)
else:
    print(f"use {sys.argv[0]} <indir> <outdir>")


def addpercentage(matchgroup) -> str:
    t = iter(matchgroup.group(1))
    newstr = '%'.join(a+b for a, b in zip(t, t))
    return '%'+newstr


def fixpath(pathstr: str) -> str:
    pathstr = pathstr.removeprefix(indir + "/")
    pattern = r"\((.+?)\)"
    pathstr = re.sub(pattern, addpercentage, pathstr)
    pathstr = urllib.parse.unquote(pathstr)
    return pathstr


parser = Parser()

ignore = ('index/current', 'WikiTipOfTheDay/current', 'BadContent/current', 'SideBar/current')
hampages = pathlib.Path(indir).glob("**/current")
for hampage in hampages:
    if str(hampage).endswith(ignore):
        continue
    current = hampage.read_text().strip()
    revisions = hampage.parent / "revisions"
    current_revision = revisions / current
    if not current_revision.exists():
        current_revision = list(revisions.iterdir())[-1]

    editlog = hampage.parent / "edit-log"
    editlog = editlog.read_text()
    timestamp = 0
    if editlog:
        lastentry = editlog.split("\n")[-2]
        timestamp = lastentry.split()[0]
    timestamp = int(timestamp) / 1000 / 1000
    timestamp = datetime.datetime.fromtimestamp(timestamp).strftime('%Y-%m-%d %H:%M:%S')

    additional_frontmatter = {}
    additional_frontmatter['moinmoin_path'] = str(current_revision).removeprefix(indir)

    body = current_revision.read_text()
    newpath = fixpath(str(current_revision))
    newpath = pathlib.Path(newpath)
    title = newpath.parent.parent.stem
    newpath = pathlib.Path(str(newpath).replace(" ", "-"))
    newpath = newpath.parent.parent.with_suffix(".md")

    outfile = outdir / newpath
    outfile.parent.mkdir(parents=True, exist_ok=True)
    outfile.write_text(parser.convert(title, body, timestamp, additional_frontmatter))

    # attachments
    attachmentdirectory = hampage.parent / "attachments"
    if attachmentdirectory.exists():
        for file in attachmentdirectory.iterdir():
            newfile = outfile.parent / file.name
            newfile.write_bytes(file.read_bytes())

