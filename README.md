convert moinmoin to hugo
========================

This is a small script that converts files from a moinmoin dump to markdown
files to be used with hugo.

Usage:
```
./convert.py <directory with moinmoin dump> <content directory for hugo files>
```
