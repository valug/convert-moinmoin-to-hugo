# SPDX-FileCopyrightText: 2023 Birger Schacht
# SPDX-License-Identifier: MIT

import importlib
import re
import pkgutil
import yaml

import parse_directives


class Parser:
    directives = None

    def __init__(self):
        self.directives = {
                name.split('.')[-1].capitalize(): importlib.import_module(name)
                for finder, name, ispkg in self.iter_namespace(parse_directives)
        }

    # this is based on  https://packaging.python.org/guides/creating-and-discovering-plugins/
    def iter_namespace(self, ns_pkg):
        # Specifying the second argument (prefix) to iter_modules makes the
        # returned name an absolute name instead of a relative one. This allows
        # import_module to work without having to do additional modification to
        # the name.
        return pkgutil.iter_modules(ns_pkg.__path__, ns_pkg.__name__ + ".")

    def convert(self, title, content, timestamp, additional_frontmatter=None):
        frontmatter = additional_frontmatter or {}
        frontmatter['title'] = title
        frontmatter['date'] = timestamp

        for key in self.directives:
            class_ = getattr(self.directives[key], key)
            instance = class_()
            content = instance.replace(content)
        content = self.link(content)
        return "{}{}\n{}".format(yaml.dump(frontmatter, indent=2, explicit_start=True, default_flow_style=False), '---', content)

    def link(self, content):
        openbrackets_re = re.compile(r'\[\[[^!]')
        match = True
        while match:
            match = re.search(openbrackets_re, content)
            if match:
                openingbracket = match.start()
                closingbracket = match.start()
                bracketcounter = 0
                for i in range(openingbracket, len(content) - 1):
                    if content[i] == '[' and content[i + 1] == '[':
                        bracketcounter = bracketcounter + 1
                    if content[i] == ']' and content[i + 1] == ']':
                        bracketcounter = bracketcounter - 1
                    if bracketcounter == 0:
                        closingbracket = i
                        break
                if closingbracket > openingbracket:
                    pipeindex = content.rfind('|', openingbracket, closingbracket)
                    if pipeindex != -1:
                        text = content[openingbracket + 2:pipeindex]
                        target = content[pipeindex + 1:closingbracket].replace(' ', '_')
                    else:
                        text = content[openingbracket + 2:closingbracket]
                        target = text.replace(' ', '_')
                    if '://' not in text:
                        text = '/' + text
                    content = content.replace(content[openingbracket:closingbracket + 2], "[{}]({})".format(target, text))
                else:
                    content = content.replace(content[openingbracket:closingbracket + 2], "[ [")
        return content
